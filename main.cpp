#include <iostream>

using namespace std;

char place[6][7];

void showField() {
    cout<< " 1 2 3 4 5 6 7\n";
    for(int a = 0; a <= 5; a++){

        for(int b =0; b <= 6; b++) cout<<" "<<place[a][b];
        cout<<'\n';

    }

}

bool check(int a, int b){
    int vertical = 1;//(|)
    int horizontal = 1;//(-)
    int diagonal1 = 1;//(\)
    int diagonal2 = 1;//(/)
    char player = place[a][b];
    int i;//vertical
    int ii;//horizontal
    //check for vertical(|)
    for(i = a +1;place[i][b] == player && i <= 5;i++,vertical++);//Check down
    for(i = a -1;place[i][b] == player && i >= 0;i--,vertical++);//Check up
    if(vertical >= 4)return true;
    //check for horizontal(-)
    for(ii = b -1;place[a][ii] == player && ii >= 0;ii--,horizontal++);//Check left
    for(ii = b +1;place[a][ii] == player && ii <= 6;ii++,horizontal++);//Check right
    if(horizontal >= 4) return true;
    //check for diagonal 1 (\)
    for(i = a -1, ii= b -1;place[i][ii] == player && i>=0 && ii >=0; diagonal1 ++, i --, ii --);//up and left
    for(i = a +1, ii = b+1;place[i][ii] == player && i<=5 && ii <=6;diagonal1 ++, i ++, ii ++);//down and right
    if(diagonal1 >= 4) return true;
    //check for diagonal 2(/)
    for(i = a -1, ii= b +1;place[i][ii] == player && i>=0 && ii <= 6; diagonal2 ++, i --, ii ++);//up and right
    for(i = a +1, ii= b -1;place[i][ii] == player && i<=5 && ii >=0; diagonal2 ++, i ++, ii --);//up and left
    if(diagonal2 >= 4) return true;
    return false;
}


int drop(int b, char player){
    if(b >=0 && b<= 6)
    {
        if(place[0][b] == '_'){
            int i;
            for(i = 0;place[i][b] == '_';i++)
                if(i == 5){place[i][b] = player;
            return i;}
            i--;
            place[i][b] =player;
            return i;

        }
        else{
            return -1;
        }

    }
    else{
        return -1;
    }

}


int main(){
   for(int a =0;a <= 5; a++){	//Erstellen des Feldes
       for(int b = 0; b<=6; b++)
           place[a][b] = '_';
   }
   showField();//Anzeige des Spielfelds
   int hold;//Spielereingabe, Reihe
   int hold2 = 0;
   int charsPlaced = 0;//Wieviele Züge es schon gab
   bool gamewon = false;
   char player = 'o';
   while(!gamewon){
       if(hold2 != -1){//Fehlerkontrolle
           if(player == 'o'){//Spielerswitch
               cout<<"Spieler 1 bitte Eingabe machen:\n" <<endl;

               player = 'x';//Kennzeichnung

           }
           else{
               cout<<"Spieler 2 bitte Eingabe machen:\n"<<endl;

               player = 'o';//Kennzeichnung
           }
       }
       while(true){ //Läuft immer, Abbruchmöglichkeiten weiter unten
           if(charsPlaced == 42) break; //Bei Unentschieden
           cin>>hold;
           cout<<"Eingabe war: "<< hold << endl; //Spielereingabe
           hold--;//Eingabe in Arraynotation umrechnen
           if(hold <=6 && hold>= 0) break;//Validation
           else cout<< "\nEingabe muss zwischen 1 und 7 sein";//Fragt jetzt neu nach Input. NaN wird nicht abgefangen.
          					

       }

       hold2 = drop(hold,player);//Sicherheitsvariable
       if(hold2 == -1)	cout<<"Reihe voll\nBitte neu wählen"<<endl;//if error -1 row is full
       else{
           cout<<"test"<<endl;
           gamewon = check(hold2,hold);//
           charsPlaced ++;//
           showField();//
       }
   }

   if(charsPlaced == 42){//if draw
       cout<<"No winner, Game was draw\n";
       cin.get(); //Zum Anhalten des Programms, besser wäre Konsole mit einer no-close-flag zu öffnen 
   return 0;
   }
   if(player == 'o')
       cout<<"Sieger: Spieler 2\n";
   else cout<<"Sieger: Spieler 1\n";
   cin.get(); //Zum Anhalten des Programms, besser wäre Konsole mit einer no-close-flag zu öffnen 
   return 0;//Ende
}


